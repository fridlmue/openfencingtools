# openFencingTools
Open Hardware and Software Designs for Fencing Tools and Devices

## Vision
In fencing a lot of money is needed for the purchase of hardware. This is a cost aspect for the athletes and clubs. In this repository open designs for useful tools for fencers shall be created.

## openMPTester (working title)
First design which is under development here is a multipurpose testing device. It shall help individuals or material maintainers from the club to find common malfunctions and errors. It shall help testing cables, weapons and other material.
