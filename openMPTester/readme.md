# openMpTester

## Req

### General

- Mobile Device (Battery powered)
- Stationary power supply support (e.g. USB power supply)
- Universal for most electric parts in fencing setups
- Easy and cheap to rebuild

### Cable 

- optically and acoustically indication of interruption
- check against FIE resistance
- in case of interruption -> "hold" mode as well as indication only when line is open/above allowed resistance
- Check, if there is cross leackage

**Modes:**

- 3pin - 3pin: Body Wire Epee: < 1 Ohm
- 3pin - 3pin: Cable Reel: < 3 Ohm
- 3pin - 3pin: Reel <-> Signalling Unit
- 3pin - (2pin; 1 clip) : Body Wire Foile/Sabre: < 1 Ohm
- 1 clip - 1 clip: Mask Wire: < 1 Ohm

### Weapon

- optically and acoustically indication of interruption/connection
- check against FIE resistance
- in case of interruption -> "hold" mode as well as indication only when line is open/above allowed resistance

**Modes:**
- Foile: Check for interruption of the connection
- Epee: Check for connection
- Sabre: Check it interruption for more than 0,1 ms
- Check, if guard blade is on ground


### West
- optically and acoustically indication of interruption
- check against FIE resistance (< 5 Ohm)


## Convention



|Plug:| | |
|---|---|---|
|L1 | -- | A |
| | | |
| | | |
|L2 | -- | B|
|L3 | -- | C|




